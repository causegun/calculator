# Calculator
Create an activity with 3 buttons to redirect to different implementations of a calculator. You can use activities or an activity with fragments.

Create the layouts of a very simple calculator using:

- FrameLayout
- LinearLayout
- ConstraintLayout

The designs of these approaches should be the same.
