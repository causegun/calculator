package com.example.calculator

import android.os.Bundle
import android.widget.EditText

class FrameLayout : Layout() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.framelayout_activity)

        val editText = findViewById<EditText>(R.id.editText)

        val buttons = arrayOf(
            R.id.button1,
            R.id.button2,
            R.id.button3,
            R.id.button4,
            R.id.button5,
            R.id.button6,
            R.id.button7,
            R.id.button8,
            R.id.button9,
            R.id.button_minus,
            R.id.button_plus,
            R.id.button_multiply
        )

        setAppendOnClickListeners(editText, buttons)
    }
}