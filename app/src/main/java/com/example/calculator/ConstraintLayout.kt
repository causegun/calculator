package com.example.calculator

import android.os.Bundle
import android.widget.EditText

class ConstraintLayout : Layout() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.constraintlayout_activity)
        val editText = findViewById<EditText>(R.id.c_editText)

        val buttons = arrayOf(
            R.id.c_button1,
            R.id.c_button2,
            R.id.c_button3,
            R.id.c_button4,
            R.id.c_button5,
            R.id.c_button6,
            R.id.c_button7,
            R.id.c_button8,
            R.id.c_button9,
            R.id.c_button_minus,
            R.id.c_button_plus,
            R.id.c_button_multiply
        )

        setAppendOnClickListeners(editText, buttons)
    }
}