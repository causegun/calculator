package com.example.calculator

import android.os.Bundle
import android.widget.EditText

class LinearLayout : Layout() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.linearlayout_activity)

        val editText = findViewById<EditText>(R.id.l_editText)

        val buttons = arrayOf(
            R.id.l_button1,
            R.id.l_button2,
            R.id.l_button3,
            R.id.l_button4,
            R.id.l_button5,
            R.id.l_button6,
            R.id.l_button7,
            R.id.l_button8,
            R.id.l_button9,
            R.id.l_button_minus,
            R.id.l_button_plus,
            R.id.l_button_multiply
        )

        setAppendOnClickListeners(editText, buttons)
    }
}