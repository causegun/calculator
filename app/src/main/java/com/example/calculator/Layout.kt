package com.example.calculator

import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

open class Layout : AppCompatActivity() {
    fun setAppendOnClickListeners(editText: EditText, buttons: Array<Int>) {
        buttons.forEach { id -> findViewById<Button>(id).setOnClickListener { editText.append((it as Button).text) } }
    }
}